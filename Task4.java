/*
 * Создание двумерного массива заполнение случайными числами и вывод на экран.
 */

/**
 *
 * @author dia1141b
 */
public class Task4 {

    public static void main(String[] args) {
        int[][] nums = new int[8][5];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 5; j++) {
// Заполнение элементов массива:
                nums[i][j] = (int) (Math.random() * 89 + 10);
// Вывод значений в одну строку:
                System.out.print(nums[i][j] + " ");
            }
// Переход на новую строку
            System.out.println();

        }
    }
}
