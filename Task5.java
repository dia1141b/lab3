


/*
 * Создать двумерный массив заполнить случайными числами
 * Поситать строку с большей по модулю суммой элементов в строке
 */
/**
 *
 * @author dia1141b
 */
import static java.lang.Math.abs;
public class Task5 {

    public static void main(String[] args) {
        int[][] nums = new int[7][4];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                // Заполнение элементов массива:
                nums[i][j] = (int) (Math.random() * 10 - 5);
                // Вывод значений в одну строку:
                System.out.print(nums[i][j] + " ");
            }
            System.out.println();
        }
        //подсчет сумм
        int a = 0;
        int b = 0;
        int c = 0;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                a = a + abs(nums[i][j]);               
            }
            if (i == 1) {
                b = a;
            }
            if (a > b) {
                b = a;             
                c = i;
            }
            a=0;
        }
        System.out.println(c);
    }
}
