/*
 * Cоздание массива нечетных чисел вывод его
 */

/**
 *
 * @author dia1141b
 */
public class Task2 {

    public static void main(String[] args) {
        int[] nums;
        nums = new int[50];
        int c = 1;
        //заполнение массива
        for (int i = 0; i < 50; i++) {
            nums[i] = c;
            c = c + 2;
        }
        //вывод в строку 
        for (int j = 0; j < 50; j++) {
            System.out.print(nums[j] + " ");
        }
        System.out.println();       

        //вывод в строку в обратном порядке
        for (int k = 49; k >= 0; k--) {
            System.out.print(nums[k] + " ");
        }

    }
}
