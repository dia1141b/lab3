/*
 *Создание массива четных чисел и вывод его
 */

/**
 *
 * @author dia1141b
 */
public class Task1 {

    public static void main(String[] args) {
        int c = 2;
        //обьявление масива nums длиной 10
        int[] nums;
        nums = new int[10];
        //заполнение массива
        for (int i = 0; i < 10; i++) {
            nums[i] = c;
            c = c + 2;
        }
        //вывод в строку 
        for (int j = 0; j < 10; j++) {
            System.out.print(nums[j] + " ");
        }
        System.out.println();
        //вывод в столбец 
        for (int a = 0; a < 10; a++) {
            System.out.println(nums[a]);
        }
    }
}
