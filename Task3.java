/*
 * Создание массива случайных элементов и вовыд его на экран
 * Нахождение  количества всех четных элементов.
 */

/**
 *
 * @author dia1141b
 */
public class Task3 {

    public static void main(String[] args) {
        int[] nums;
        nums = new int[15];
        //создание массива и заполнение и вывод на экран
        for (int i = 0; i < 15; i++) {
            nums[i] = (int) (Math.random() * 10);
            System.out.print(nums[i] + " ");
        }
        System.out.println();
        int k = 0;
        //поиск кол-ва четных элементов массива
        for (int j = 0; j < 15; j++) {
            if (nums[j] % 2 == 0) {
                k = k + 1;
            }
        }
        System.out.print(k);

    }
}
